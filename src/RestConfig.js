import axios from "axios";

let instance = axios.create({
    headers: {
        "Access-Control-Allow-Origin": "*"
    },
    timeout:10000
});


class BasicRequest{
    async create(url,body){
        return await instance.post(url,body)
    }
    async read(url){
        return await instance.get(url)
    }
    async update(url,body){
        return await instance.put(url,body)
    }
    async delete(url){
        return await instance.delete(url)
    }
}

let Requests = new BasicRequest();
export default Requests;