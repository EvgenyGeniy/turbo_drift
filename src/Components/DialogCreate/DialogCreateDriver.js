import React, {useEffect, useState} from "react"
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Requests from "../../RestConfig";
import AddIcon from "@material-ui/icons/Add"
import {MenuItem, Select} from "@material-ui/core";
import InputLabel from '@material-ui/core/InputLabel';

export const DialogCreateDriver = ({url, open, onClose, fieldsTitle}) => {
    const [teams, setTeams] = useState([]);
    const [data, setData] = useState({
        initials: '',
        city: '',
        instagram: '',
        license: '',
        startNum: '',
        team: null
    });
    const inputs = () => {
        return fieldsTitle.map((elem, index) => {
            return <TextField
                required={true}
                style={{
                    marginTop: '20px'
                }}
                variant="outlined" label={elem} value={data[Object.keys(data)[index]]} onChange={e => {
                setData({...data, [Object.keys(data)[index]]: e.target.value});
            }
            }/>
        })
    };

    useEffect(() => {
        Requests.read('/api/team/scores').then(res => {
            setTeams(res.data);
        });
    }, []);

    const getTeams = () => {
        return teams.map(elem => {
            return <MenuItem key={elem.id} value={elem.id}>{elem.name}</MenuItem>
        })
    }


    const handleSubmit = () => {
        console.log(data)
        Requests.create(url, data).then(() => {
            onClose();
        }, () => {
            window.alert("Ошибка создания")
        });
    };

    const handleChange = (event) => {
        setData({...data, team: {id: event.target.value}});
    };

    return (<Dialog open={open} onClose={onClose}>
        <div style={{display: "flex", flexDirection: "column", justifyContent: "center", padding: "30px"}}>
            {inputs()}
            <InputLabel id="team" style={{
                marginTop: '10px'
            }}>Название команды</InputLabel>
            <Select
                defaultValue={'Team_1'}
                onChange={handleChange}
                style={{
                    marginTop: '5px'
                }}
                variant='outlined'
                label='Команда'
                labelId="team"
            >
                {getTeams()}
            </Select>
            <Button style={{marginTop: '20px'}} variant="contained" color="primary"
                    onClick={handleSubmit}><AddIcon/></Button>
        </div>
    </Dialog>)
};