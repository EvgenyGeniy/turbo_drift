import React, {useState} from "react"
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Requests from "../../RestConfig";
import AddIcon from "@material-ui/icons/Add"

export const DialogCreateTeam = ({url, open, onClose, fieldsTitle}) => {
    const [data, setData] = useState({
        name: ''
    });
    const inputs = () => {
        return fieldsTitle.map((elem) => {
            return <TextField
                required={true}
                style={{
                    marginTop: '20px'
                }}
                variant="outlined" label={elem} value={data.name} onChange={e => {
                setData({name: e.target.value});
            }
            }/>
        })
    };

    const handleSubmit = () => {
        Requests.create(url, data).then(() => {
            onClose();
        },() => {
            window.alert("Ошибка создания")
        });
    };
    return (<Dialog open={open} onClose={onClose}>
        <div style={{display: "flex", flexDirection: "column", justifyContent: "center", padding: "10px"}}>
            {inputs()}
            <Button style={{marginTop:'20px'}} variant="contained" color="primary" onClick={handleSubmit}><AddIcon/></Button>
        </div>
    </Dialog>)
};