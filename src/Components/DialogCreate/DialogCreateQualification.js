import React, {useEffect, useState} from "react"
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Requests from "../../RestConfig";
import AddIcon from "@material-ui/icons/Add"
import {MenuItem, Select} from "@material-ui/core";
import InputLabel from '@material-ui/core/InputLabel';

export const DialogCreateQualification = ({url, open, onClose, fieldsTitle}) => {
    const [drivers,setDrivers] = useState([]);
    const [data, setData] = useState({
        stageNumber:'',
        place:'',
        best:'',
        worst:'',
        points:'',
        initials:''
    });
    const inputs = () => {
        return fieldsTitle.map((elem,index) => {
            return <TextField
                required={true}
                style={{
                    marginTop: '20px'
                }}
                variant="outlined" label={elem} value={data[Object.keys(data)[index]]} onChange={e => {
                setData({...data,[Object.keys(data)[index]]: e.target.value});
            }
            }/>
        })
    };

    useEffect(()=>{
        Requests.read('/api/driver/all').then(res=>{
             setDrivers(res.data)
        })
    },[])

    const getDrivers = () =>{
        return drivers.map(elem=>{
            return <MenuItem key={elem.id} value={elem.id}>{elem.initials}</MenuItem>
        })
    }


    const handleSubmit = () => {
        Requests.create(url, data).then(() => {
            onClose();
        },() => {
            window.alert("Ошибка создания")
        });
    };

    const handleChange = (event) => {
        setData({...data,driver:{id:event.target.value}});
    };

    return (<Dialog open={open} onClose={onClose}>
        <div style={{display: "flex", flexDirection: "column", justifyContent: "center", padding: "30px"}}>
            {inputs()}
            <InputLabel id="team" style={{
                marginTop:'10px'
            }}>Водитель</InputLabel>
            <Select
                onChange={handleChange}
                style={{
                    marginTop:'5px'
                }}
                variant='outlined'
                label='Команда'
                labelId="team"
            >
                {getDrivers()}
            </Select>
            <Button style={{marginTop:'20px'}} variant="contained" color="primary" onClick={handleSubmit}><AddIcon/></Button>
        </div>
    </Dialog>)
};