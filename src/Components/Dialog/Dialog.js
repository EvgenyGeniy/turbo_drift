import React, {useEffect, useState} from "react";
import {Dialog} from "@material-ui/core";
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import Requests from "../../RestConfig";


export const DialogComponent = ({open, onClose, data, fieldsTitle, url, editable, onEdit}) => {
    const [edit, setEdit] = useState(data);
    const [id, setId] = useState();

    useEffect(() => {
        setId(data.id);
        delete data.id;
    }, []);

    const inputs = () => {
        let arr = Object.values(data);
        arr.pop();
        return arr.map((elem, index) => {
            return (
                <TextField
                    style={{
                        marginTop: '20px'
                    }}
                    onChange={(e) => {
                        setEdit({
                            ...edit,
                            [Object.keys(data)[index]]: e.target.value
                        })
                    }
                    }
                    InputProps={{
                        readOnly: !editable[index],
                    }}
                    variant="outlined"
                    label={fieldsTitle[index]}
                    value={edit[Object.keys(data)[index]]}
                />
            )
        })
    }


    const handleDelete = () => {
        Requests.delete(url + data.id)
    };


    return (
        <Dialog open={open} onClose={onClose} style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '10px'
        }}>
            {!!data &&
            <div style={{
                padding: '60px',
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column'
            }}>
                {!!data && inputs()}
                {/*{<Button style={{marginTop:'20px'}} variant="contained" color="primary" onClick={handleDelete}><DeleteIcon/></Button>*/}
                {onEdit !== undefined && <Button style={{marginTop: '20px'}} variant="contained" color="primary"
                        onClick={() => onEdit(id, edit.name)}><CreateIcon/></Button>}
            </div>}
        </Dialog>
    )
}