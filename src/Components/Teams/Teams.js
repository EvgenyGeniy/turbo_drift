import MaterialTable from 'material-table';
import React, {useEffect, useState} from "react";
import Requests from "../../RestConfig";
import {DialogComponent} from "../Dialog/Dialog";
import {DialogCreateTeam} from "../DialogCreate/DialogCreateTeam";




export const Teams = () =>{
    const [data,setData] = useState();
    const [open,setOpen] = useState(false);
    const [openCreate, setOpenCreate] = useState(false);
    const [currentRow,setCurrentRow] = useState({});
    const columns = [
        { title: 'Название', field: 'name' }
    ]

    const handleClick = (event,rowData) =>{
        setOpen(true);
        setCurrentRow(rowData)
    }

    const teamData = (arr) => {
        return arr.map(elem => {
                let team = {
                    id : elem.id,
                    name: elem.name,
                    totalScore: elem.totalScore == null ? 0 : elem.totalScore
                };
                elem.stages.forEach(stage => {
                    switch (stage.name) {
                        case "1":
                            team.firstStage = stage.score;
                            break;
                        case "2":
                            team.secondStage = stage.score;
                            break;
                        case "3":
                            team.thirdStage = stage.score;
                            break;
                        case "4":
                            team.fourthStage = stage.score;
                            break;
                        case "5":
                            team.fifthStage = stage.score;
                            break;
                    }
                });
            return team;
        })
    };

    const handleEdit = (id, name) =>{
        Requests.update("/api/team", {
            id:id,
            name: name
        });
    };


    useEffect(()=>{
        Requests.read('/api/team/scores').then(res=>{
            setData(teamData(res.data));
        });
    },[openCreate]);

    return(
        <div style={{
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            marginTop:'100px'
        }}>
            <DialogCreateTeam
                url={"api/team"}
                open={openCreate}
                onClose={() => setOpenCreate(false)}
                fieldsTitle={['Название']}
            />
            {open && <DialogComponent
                url = {"api/team/?id="}
                open={open}
                onClose={()=>setOpen(false)}
                data={currentRow}
                editable={[true,0,0,0,0,0,0]}
                fieldsTitle={['Название','Итоговый счет','Первый этап','Второй этап','Третий этап','Четвертый этап']}
                onEdit={handleEdit}
            />}
            <MaterialTable
                style={{width:'70%'}}
                title="Команды"
                columns={columns}
                data={data}
                actions={[
                    {
                        icon: 'add',
                        tooltip: 'Add User',
                        isFreeAction: true,
                        onClick: () => setOpenCreate(true)
                    }
                ]}
                onRowClick={handleClick}
            />
        </div>
    )
}