import MaterialTable from 'material-table';
import React, {useEffect, useState} from "react";
import Requests from "../../RestConfig";
import {DialogComponent} from "../Dialog/Dialog";
import {DialogCreateAuto} from "../DialogCreate/DialogCreateAuto";
import {DialogCreateDriver} from "../DialogCreate/DialogCreateDriver";




export const Cars = () =>{
    const [data,setData] = useState([]);
    const [open,setOpen] = useState(false);
    const [currentRow,setCurrentRow] = useState(null);
    const [openCreate, setOpenCreate] = useState(false);
    const columns = [
        { title: 'Наименование', field: 'mark'},
        { title: 'Двигатель', field: 'engine'},
        { title: 'Водитель', field: 'initials'}
    ];


    const handleClick = (event,rowData) =>{
        setOpen(true);
        setCurrentRow(rowData)
    };

    const autosData = (arr) => {
        return arr.map(elem => {
            let auto = {
                initials: elem.driver.initials,
                mark: elem.mark,
                engine: elem.engine,
                power: elem.power,
                engineVolume: elem.engineVolume,
                tireMark: elem.tireMark,
            };
            return auto;
        });
    };


    useEffect(()=>{
        Requests.read('/api/auto/all').then(res=>{
            setData(autosData(res.data));
        })
    },[openCreate]);

    return(
        <div style={{
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            marginTop:'100px'
        }}>
            {open && <DialogComponent
                open={open}
                onClose={() => setOpen(false)}
                data={currentRow}
                fieldsTitle={['Водитель', 'Машина', 'Двигатель', 'Л.С', 'Объем', 'Шины', ]}
                editable={[false, true, true, true, true, true]}
            />}
            <DialogCreateAuto
                url={"api/auto"}
                open={openCreate}
                onClose={()=>setOpenCreate(false)}
                fieldsTitle={['Машина','Двигатель','Л.С','Объем','Шины']}
            />
            <MaterialTable
                style={{width:'70%'}}
                title="Машины"
                columns={columns}
                data={data}
                onRowClick={handleClick}
                actions={[
                    {
                        icon: 'add',
                        tooltip: 'Add User',
                        isFreeAction: true,
                        onClick: () => setOpenCreate(true)
                    }
                ]}
            />
        </div>
    )
}