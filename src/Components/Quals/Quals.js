import MaterialTable from 'material-table';
import React, {useEffect, useState} from "react";
import Requests from "../../RestConfig";
import {DialogComponent} from "../Dialog/Dialog";
import {DialogCreateQualification} from "../DialogCreate/DialogCreateQualification";


export const Quals = () => {
    const [data, setData] = useState([]);
    const [open, setOpen] = useState(false);
    const [currentRow, setCurrentRow] = useState(null);
    const [openCreate, setOpenCreate] = useState(false);
    const columns = [
        {title: 'Номер этапа', field: 'stageNumber'},
        {title: 'Пилот', field: 'initials',}
    ]

    const handleClick = (event, rowData) => {
        setOpen(true);
        setCurrentRow(rowData)
    }


    const qualsData = (arr) => {
        return arr.map((elem, i, array) => {
            const driver = typeof elem.driver === "number" ?
                array
                    .map(a => a.driver)
                    .filter(a => typeof a !== "number")
                    .find(a => a.id === elem.driver)
                : elem.driver;
            let qual = {
                stageNumber: elem.stageNumber,
                initials: driver.initials,
                place: elem.place,
                best: elem.best,
                worst: elem.worst,
                points: elem.points
            };
            return qual;
        })
    };

    useEffect(() => {
        Requests.read('/api/qualification/all').then(res => {
            setData(qualsData(res.data))
        });
    }, [openCreate]);

    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '100px'
        }}>
            {open && <DialogComponent
                fieldsTitle={['Номер этапа', 'Водитель', 'Место в квалификации', 'Лучший', 'Худший', 'Очки']}
                open={open}
                onClose={() => setOpen(false)}
                data={currentRow}
                editable={[false, false, true, true, true, true]}
            />}
            <DialogCreateQualification
                url={"api/qualification"}
                open={openCreate}
                onClose={() => setOpenCreate(false)}
                fieldsTitle={['Номер этапа', 'Место в квалификации', 'Лучший', 'Худший', 'Очки']}
            />
            <MaterialTable
                style={{width: '70%'}}
                title="Квалификации"
                columns={columns}
                data={data}
                onRowClick={handleClick}
                actions={[
                    {
                        icon: 'add',
                        tooltip: 'Add User',
                        isFreeAction: true,
                        onClick: () => setOpenCreate(true)
                    }
                ]}
            />
        </div>
    )
}