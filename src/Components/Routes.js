import React from 'react'
import {BrowserRouter} from "react-router-dom";
import {Route} from 'react-router'
import {Navbar} from "./Navbar/Navbar";
import {Drivers} from "./Drivers/Drivers";
import {Teams} from "./Teams/Teams";
import {Cars} from "./Cars/Cars";
import {Stages} from "./Stages/Stages";
import {Quals} from "./Quals/Quals";




export const Routes = () =>{
    return(
        <div>
            <BrowserRouter>
                <Navbar/>
                <Route path='/drivers' component={Drivers}/>
                <Route path='/teams' component={Teams}/>
                <Route path='/cars' component={Cars}/>
                <Route path='/quals' component={Quals}/>
                <Route path='/stages' component={Stages}/>
            </BrowserRouter>
        </div>
    )
}