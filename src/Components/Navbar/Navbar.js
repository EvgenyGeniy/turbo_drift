import React from "react";
import {Tabs, Tab, Paper} from '@material-ui/core'
import {Link} from 'react-router-dom'

export const Navbar = () =>{
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return(
        <Paper>
            <Tabs
                style={{
                    width:'100%',
                    height:'70px'
                }}
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                centered
            >
                <Tab style={{
                    width:'25%',
                    height:'70px'
                }} label="Водители" component={Link} to='/drivers'/>
                <Tab style={{
                    width:'25%',
                    height:'70px'
                }} label="Машины" component={Link} to='/cars'/>
                <Tab style={{
                    width:'25%',
                    height:'70px'
                }} label="Команды" component={Link} to='/teams'/>
                <Tab style={{
                    width:'25%',
                    height:'70px'
                }} label="Квалификации" component={Link} to='/quals'/>
                <Tab style={{
                    width:'25%',
                    height:'70px'
                }} label="Этапы" component={Link} to='/stages'/>
            </Tabs>
        </Paper>
    )
}