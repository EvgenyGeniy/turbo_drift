import MaterialTable from 'material-table';
import React, {useEffect, useState} from "react";
import Requests from "../../RestConfig";
import {DialogComponent} from "../Dialog/Dialog";
import {DialogCreateStage} from "../DialogCreate/DialogCreateStage";
import {DialogCreateQualification} from "../DialogCreate/DialogCreateQualification";




export const Stages = () =>{
    const [data,setData] = useState([]);
    const [open,setOpen] = useState(false);
    const [currentRow,setCurrentRow] = useState(null);
    const [openCreate, setOpenCreate] = useState(false);
    const columns = [
        { title: 'Номер этапа', field: 'stageNumber' },
        { title: 'Пилот',field: 'initials'}
    ];

    const handleClick = (event,rowData) =>{
        setOpen(true);
        setCurrentRow(rowData)
    };

    const stagesData = (arr) => {
        return arr.map(elem => {
            let stage = {
                initials: elem.qualification.driver.initials,
                stageNumber: elem.qualification.stageNumber,
                tanso: elem.tanso,
                cuiso: elem.cuiso,
                points: elem.qualification.points,
            };
            return stage;
        })
    };


    useEffect(()=>{
        Requests.read('/api/result/all').then(res=>{
            setData(stagesData(res.data));
        })

    },[openCreate]);

    return(
        <div style={{
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            marginTop:'100px'
        }}>
            <DialogCreateStage
                url={"api/result"}
                open={openCreate}
                onClose={()=>setOpenCreate(false)}
                fieldsTitle={['Тансо','Цуйсо','Место в заезде']}
            />
            { open && <DialogComponent open={open}
                              fieldsTitle={['Водитель','Номер этапа', 'Тансо', 'Цуйсо', 'Очки',]}
                              onClose={() => setOpen(false)}
                              editable={[false, false, true, true, true]}
                              data={currentRow}/>}
            <MaterialTable
                style={{width:'70%'}}
                title="Этапы"
                columns={columns}
                data={data}
                onRowClick={handleClick}
                actions={[
                    {
                        icon: 'add',
                        tooltip: 'Add User',
                        isFreeAction: true,
                        onClick: () => setOpenCreate(true)
                    }
                ]}
            />
        </div>
    )
}