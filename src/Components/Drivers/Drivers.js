import MaterialTable from 'material-table';
import React, {useEffect, useState} from "react";
import Requests from "../../RestConfig";
import {DialogComponent} from "../Dialog/Dialog";
import {DialogCreateDriver} from "../DialogCreate/DialogCreateDriver";




export const Drivers = () =>{
    const [data,setData] = useState();
    const [open,setOpen] = useState(false);
    const [currentRow,setCurrentRow] = useState(null);
    const [openCreate, setOpenCreate] = useState(false);
    const columns = [
        { title: 'Пилот', field: 'initials' },
        { title: 'Команда', field: 'team'},
    ];

    const handleClick = (event,rowData) =>{
        setCurrentRow(rowData);
        setOpen(true);
    };

    const driversData = (arr) => {
        return arr.map(elem => {
            let driver = {
                initials: elem.initials,
                city: elem.city,
                instagram: elem.instagram,
                team: elem.team.name,
                license: elem.license,
                startNum: elem.startNum
            };
            return driver;
        })
    };


    useEffect(()=>{
      Requests.read('/api/driver/all').then(res=>{
          setData(driversData(res.data));
      })
    },[openCreate]);

    return(
        <div style={{
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            marginTop:'100px'
        }}>
            {open && <DialogComponent
                fieldsTitle={['Водитель', 'Город', 'Instagram', 'Команда', 'Лицензия', 'Стартовый номер']}
                open={open}
                onClose={() => setOpen(false)}
                data={currentRow}
                editable={[true, true, true, false, true, false]}
            />}
            <DialogCreateDriver
                url={"api/driver"}
                open={openCreate}
                onClose={()=>setOpenCreate(false)}
                fieldsTitle={['Водитель','Город','Instagram','Лицензия','Стартовый номер']}
            />
            <MaterialTable
                style={{width:'70%'}}
                title="Водители"
                columns={columns}
                data={data}
                onRowClick={handleClick}
                actions={[
                    {
                        icon: 'add',
                        tooltip: 'Add User',
                        isFreeAction: true,
                        onClick: () => setOpenCreate(true)
                    }
                ]}
            />
        </div>
    )
}